from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from . import validator
from .models import Items
import requests
import concurrent.futures
import time

def home(request):
    return render(request, 'temp_app1/home.html')

def addItem(request):
    itemEntered = request.POST.get('itemName')
    itemName = validator.Validate(itemEntered)
    validationObject = itemName.validate()
    if validationObject:
        addedItem = Items.objects.create(name=itemEntered)
        return JsonResponse({'item' : addedItem.name, 'status' : addedItem.status , 'id':addedItem.id})
    return JsonResponse({'message' : "Invalid Value. Please enter something from book, pen, folder and bag"})


def fetchURL(url:str):
    requests.get(url)


def delayFunction(request):
    delayValue = request.GET.get('delay')
    try:
        int(delayValue)
    except:
        return JsonResponse({"message": "Kindly enter an integer value only."})
    URL = 'https://httpbin.org/delay/{}'.format(delayValue)
    startTime = time.time()
    with concurrent.futures.ThreadPoolExecutor() as tpe:
        [tpe.submit(fetchURL, URL) for _ in range(5)]
    endTime = time.time()
    timeTaken = (round(endTime-startTime, 2))
    return JsonResponse({'time taken': timeTaken})

