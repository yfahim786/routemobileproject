from marshmallow import Schema, fields, validate

class ItemSchema(Schema):
    item = fields.String(validate = lambda item : True if item in ['book', 'pen', 'folder', 'bag'] else False)


class Validate():

    def __init__(self, itemName):
        self.itemName = itemName

    def validate(self):
        item = {}
        item["item"] =  self.itemName
        schema = ItemSchema()
        try:
            schema.load(item)
            return True
        except:
            return False