from django.db import models

# Create your models here.
class Items(models.Model):
    name = models.CharField(max_length= 50, blank = True, help_text='Name')
    status = models.CharField(max_length= 10, blank = True, default= "Pending", help_text='Status')

    def __str__(self):
        return self.name