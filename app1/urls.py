from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from app1 import views


urlpatterns = [
    path('', views.home, name="home"),
    path('addItem/', views.addItem, name="addItem"),
    path('delayFunction/', views.delayFunction, name="delayFunction"),
]
